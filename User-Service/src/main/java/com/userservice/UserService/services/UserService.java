package com.userservice.UserService.services;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.respostiories.UserRespository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 *=============================================================
 * This service class is used to handle all the business logic
 * related to users within the application
 *=============================================================
 *
 * @author  Naresh Shanmugaraj
 *
 * @version 1.0
 * @since   2021-12-11
 */
@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final RestTemplateBuilder restTemplate;
    private final UserRespository repository;
    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;
    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    public UserService(UserRespository repository, RestTemplateBuilder builder) {
        this.repository = repository;
        this.restTemplate = builder;
    }

    /**
     * ==============================================
     * This method is used to create a new user
     * =============================================
     *
     * @param user userDTO
     * @return ResponseEntity with a success or error message
     * @since 1.0
     */
    public ResponseEntity<String> createUser(UserDTO user) {
        LOGGER.info("==========Entered into createUser method in UserService============");

        try {
            UserEntity userEntity = new UserEntity(user.getName(), user.getAge());
            repository.save(userEntity);

            LOGGER.info("User created successfully: {}", userEntity.getName());

            return new ResponseEntity<>(
                    "User created successfully!",
                    HttpStatus.CREATED
            );
        } catch (Exception e) {
            LOGGER.error("ERROR creating user : {}", e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * ===============================================
     * This method is used to update an existing user
     * ===============================================
     *
     * @param user userDTO
     * @return ResponseEntity with a success or error message
     * @since 1.0
     */
    public ResponseEntity<String> updateUser(UserDTO user) {
        LOGGER.info("==========Entered into updateUser method in UserService============");

        if (user.getId() == null)
            return new ResponseEntity<>("ID can't be empty", HttpStatus.BAD_REQUEST);

        try {
            UserEntity userEntity = repository.getById(user.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);

            LOGGER.info("User updated successfully: {}", userEntity.getName());

            return new ResponseEntity<>(
                    "User updated successfully!",
                    HttpStatus.OK
            );
        } catch (Exception e) {
            LOGGER.error("ERROR updating user : {}", e.getMessage());
            return new ResponseEntity<>(
                    "Something went wrong!",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
    }


    /**
     * ================================================
     * This method is used to get a list of all users
     * ===============================================
     *
     * @return ResponseEntity with a list of users or error
     * @since 1.0
     */
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        LOGGER.info("==========Entered into getAllUsers method in UserService============");

        List<UserDTO> users = null;

        try {
            users = repository.findAll()
                    .stream()
                    .map(user -> new UserDTO(
                            user.getId(),
                            user.getName(),
                            user.getAge()
                    )).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error("ERROR fetching users : {}", e.getMessage());
            return new ResponseEntity<>(
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    /**
     * ================================================================
     * This method is used to get a list of orders for a given userId
     * ================================================================
     *
     * @param id userId
     * @return ResponseEntity with a list of orders or error message
     * @since 1.0
     */
    public ResponseEntity<List<OrderDTO>> getOrdersByUserId(String id) {
        LOGGER.info("==========Entered into getOrdersByUserId method in UserService============");

        List<OrderDTO> orders = null;
        try {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/" + id),
                    List.class
            );
        } catch (Exception e) {
            LOGGER.error("ERROR fetching orders by userId : {}", e.getMessage());
            return new ResponseEntity<>(
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

}