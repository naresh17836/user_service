package com.userservice.UserService.respostiories;

import com.userservice.UserService.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRespository extends JpaRepository<UserEntity, Long> {

}
