package com.userservice.UserService.controllers;

import com.userservice.UserService.dto.OrderDTO;
import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/user")

public class UserController {

    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<UserDTO>> getAllUsers(){
        return service.getAllUsers();
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public ResponseEntity<List<OrderDTO>> getOrdersByUserId(@PathVariable final String id){
        return service.getOrdersByUserId(id);
    }

    @PostMapping("/create")
    public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO data){
        return service.createUser(data);
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateUser(@Valid @RequestBody UserDTO data) {
        return service.updateUser(data);
    }
}