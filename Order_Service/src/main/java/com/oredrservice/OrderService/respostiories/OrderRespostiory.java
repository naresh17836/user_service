package com.oredrservice.OrderService.respostiories;

import com.oredrservice.OrderService.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRespostiory extends JpaRepository<OrderEntity, Long> {

    @Query("SELECT o from OrderEntity o where o.userId = ?1 ")
    List<OrderEntity> findOrdersByUserId(Long id);
}