package com.oredrservice.OrderService.dtos;

public class OrderDTO {
    private Long id;
    private Long userId;
    private String orderId;

    public OrderDTO() {
    }

    public OrderDTO(Long id, Long userId, String orderId) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
    }

    public OrderDTO(Long id, String orderId, Long userId) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
