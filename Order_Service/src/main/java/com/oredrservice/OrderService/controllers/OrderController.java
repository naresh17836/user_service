package com.oredrservice.OrderService.controllers;

import com.oredrservice.OrderService.dtos.OrderDTO;
import com.oredrservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/order")

public class OrderController {

    private final OrderService service;

    @Autowired
    public OrderController(OrderService service) {
        this.service = service;
    }

    @GetMapping("/getAllOrders")
    public List<OrderDTO>getAllOrders(){
        return service.getAllOrders();

    }

    @PostMapping("/create")
    public ResponseEntity<String> createOrder(@Valid @RequestBody OrderDTO data) {
        return service.createOrder(data);
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateOrder(@Valid @RequestBody OrderDTO data) {
        return service.updateOrder(data);
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public ResponseEntity<List<OrderDTO>> getOrdersByUserId(@PathVariable final Long id) {
        return service.getOrdersByUserId(id);
    }


}